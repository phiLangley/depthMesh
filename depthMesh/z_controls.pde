float ROTX = 0;                        
float ROTY = 0;
float SCALE = 0.7;

boolean drawR=true;
boolean drawB=true;
boolean drawG=true;
boolean drawI=true;
boolean save3D=false;
boolean save2D=false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.1;
       } else {
          SCALE = SCALE-0.1;
       }
//       //if (SCALE<0.1){ SCALE=0.1;}
//       //if (SCALE>=10){ SCALE=10;}
     }
     redraw();
   
}

void keyReleased(){
  
  if (key == 'm'||key =='M') {
    save3D=true;
    println("exporting 3D...");
    redraw();
  }
  
  if (key == 'i'||key =='I') {
    save2D=true; 
    println("exporting 2D...");
    redraw();
  } 
}




