void getPix(){
  
  pixData = loadImage(fileName);
  clear();
  image(pixData, 0, 0);
  pWidth = pixData.width;
  pHeight = pixData.height;
  
  myMeshArray = new pointObject[pWidth][pHeight];
  
  loadPixels();

  for (int x = 0; x<pixData.width; x++){
    for (int y = 0; y<pixData.height; y++){
      color tempCol = get(x,y);
      myMeshArray[x][y] = new pointObject(tempCol, x, y);
    }
  }
  
}


