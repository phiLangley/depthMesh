class pointObject{

  color col;
  
  int posX;
  int posY;
  int posZ;
  
  int r;
  int g;
  int b;
  
  pointObject(color c, int pX, int pY){
    
    col=c;
    posX = pX-(pixData.width/2);
    posY = pY-(pixData.height/2);
    
    r = int(red(c));
    g = int(green(c));
    b = int(blue(c)); 
    
   posZ = r-g-b;
    
  }
  
  void drawPoint(){
    
    strokeWeight(1/SCALE);
    stroke(r,g,b);  
    point(posX, posY, posZ);  
        
  }
  
  void drawTriangle(int v1, int v2, int v3, int v4, int v5, int v6, int v7, int v8, int v9){
    
    strokeWeight(1/SCALE);
    stroke(r,g,b);
    fill(r,g,b);
    
    beginShape();
      vertex(posX, posY, posZ);
      vertex(v1, v2, v3);   
      vertex(v4, v5, v6);
    endShape(CLOSE);
    
    beginShape();
      vertex(v1, v2, v3);  
      vertex(v4, v5, v6);
      vertex(v7, v8, v9);
    endShape(CLOSE);  
  }
 
}

////////////////////////////////////////////////////////
void drawPoints(){
  
  for (int x = 0; x<pixData.width-res; x+=res){
    for (int y = 0; y<pixData.height-res; y+=res){
      myMeshArray[x][y].drawPoint();
    }
  }
}

void drawMesh(){
  for (int x = 0; x<pixData.width-res; x+=res){
    for (int y = 0; y<pixData.height-res; y+=res){
      
       int v1x = myMeshArray[x+res][y].posX;
       int v1y = myMeshArray[x+res][y].posY;
       int v1z = myMeshArray[x+res][y].posZ;
       
       int v2x = myMeshArray[x][y+res].posX;
       int v2y = myMeshArray[x][y+res].posY;
       int v2z = myMeshArray[x][y+res].posZ;

       int v3x = myMeshArray[x+res][y+res].posX;
       int v3y = myMeshArray[x+res][y+res].posY;
       int v3z = myMeshArray[x+res][y+res].posZ;

       myMeshArray[x][y].drawTriangle(v1x, v1y, v1z, v2x, v2y, v2z, v3x, v3y, v3z);  
       
    }
  }
}

