depthMesh

A short processing sketch for creating a mesh from a 2D depth image....

The code uses the RGB data of each pixel in a depth sensor image and will generate either a 'point cloud' or a triangulated mesh. 

In the code, you have to;
specify the image file and loaction
specify the 'resolution' of the model
choose whether to uses the points or the mesh 
(look for the annotations...)

CONTROLS

L MOUSE = orbit
R MOUSE CLICK AND DRAG UP = zoom in
R MOUSE CLICK AND DRAG DOWN = zoom out

PRESS'i' = export depthMesh/image0.png

if you have chosen to display a mesh...
PRESS'm' = export depthMesh/data/modelExport.obj



